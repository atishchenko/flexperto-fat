<?php

use yii\db\Migration;

class m160315_160432_user_phone_column extends Migration
{
    public function up()
    {
        $this->addColumn('{{%user}}', 'phone', $this->string() . ' AFTER email');
    }

    public function down()
    {
        $this->dropColumn('{{%user}}', 'phone');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
