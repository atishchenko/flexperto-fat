<?php

use yii\db\Migration;

class m160317_103109_user_views_column extends Migration
{
    public function up()
    {
        $this->addColumn('{{%user}}', 'views', $this->integer()->unsigned()->defaultValue(0) . ' AFTER status');
    }

    public function down()
    {
        $this->dropColumn('{{%user}}', 'views');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
