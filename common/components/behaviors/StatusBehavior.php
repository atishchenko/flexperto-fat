<?php
/**
 * Created by PhpStorm.
 * @author: Alexey Tishchenko
 * @email: alexey@atishchenko.com
 * @UpWork: https://www.upwork.com/freelancers/~01ad7ed1a6ade4e02e
 * @date: 15.03.16
 */

namespace common\components\behaviors;
/**
 * Trait StatusBehavior implements "status" attribute representation
 * @see \sjoorm\yii\components\ requires usage of
 * @package common\components\behaviors
 *
 * @static array $bootstrapStatuses
 * @property string $status
 * @property string $statusLabel
 * @property string $statusClass
 *
 * @method array getConstants($prefix)
 */
trait StatusBehavior {

    /**
     * Gets bootstrap statuses
     * @return array
     */
    public abstract function getBootstrapStatuses();

    /**
     * @return string
     */
    public function getStatusClass() {
        $classes = $this->getBootstrapStatuses();
        return isset($classes) ? $classes[$this->status] : 'default';
    }

    /**
     * @return string
     */
    public function getStatusLabel() {
        return "<span class=\"label label-{$this->statusClass}\">{$this->status}</span>";
    }
}
