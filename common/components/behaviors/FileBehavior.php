<?php
/**
 * Created by PhpStorm.
 * @author: Alexey Tishchenko
 * @email: alexey@atishchenko.com
 * @UpWork: https://www.upwork.com/freelancers/~01ad7ed1a6ade4e02e
 * @date: 16.03.16
 */
namespace common\components\behaviors;
use yii\base\Exception;
use yii\base\InvalidParamException;
use yii\helpers\FileHelper;
use yii\helpers\Url;

/**
 * Trait FileBehavior implements operations with some model's file
 * @package common\components\behaviors
 *
 * @property integer $id
 * @property string $file
 * @property boolean $isNewRecord
 * @method void addError(string $attribute, string $error)
 * @method static string className()
 * @method boolean save($runValidation = true, $attributeNames = null)
 *
 * @property string $assetsUri URI to published files directory
 * @property string $assetsPath path to published files directory
 * @property string $dataPath path to data storage root directory
 */
trait FileBehavior {

    /**
     * Returns URI to published files directory
     * @return string
     */
    protected abstract function getAssetsUri();

    /**
     * Returns path to published files directory
     * @return string
     */
    protected abstract function getAssetsPath();

    /**
     * Returns path to data storage root directory
     * @return string
     */
    protected abstract function getDataPath();

    /**
     * Gets backslash-filtered fully-qualified class name
     * @return mixed
     */
    protected static function classNamePurified() {
        return str_replace('\\', '-', static::className());
    }

    /**
     * Gets directory path containing current model's file
     * @return string
     */
    protected function getDir() {
        return "{$this->dataPath}/" . static::classNamePurified() . "/{$this->id}";
    }

    /**
     * Gets full path to the current model's file
     * @return string
     */
    public function getPath() {
        return $this->getDir() . "/{$this->file}";
    }

    /**
     * Prepares directory for current model's file
     * @return boolean
     */
    protected function prepareDir() {
        if(file_exists($this->getDir())) {
            return true;
        } else {
            try {
                return FileHelper::createDirectory($this->getDir());
            } catch(Exception $e) {
                \Yii::warning($e->getMessage(), 'FileBehavior');
                return false;
            }
        }
    }

    /**
     * Publishes model's file dir to specified assets folder visible under website dir
     * @param boolean $scheme if returned URL should contain website name and schema
     * @return string
     */
    protected function publishDir($scheme = false) {
        $hash = md5($this->id . \Yii::$app->id);
        $container = "{$this->assetsPath}/" . static::classNamePurified();
        try {
            FileHelper::createDirectory("{$this->assetsPath}/" . static::classNamePurified());
        } catch (\Exception $e) {
            \Yii::warning($e->getMessage(), 'FileBehavior');
        }
        $dir = "$container/$hash";
        if(!file_exists($dir)) {
            @symlink($this->getDir(), $dir);
        }

        return Url::base($scheme) . "{$this->assetsUri}/". static::classNamePurified() . "/$hash";
    }

    /**
     * Gets model's file unique URL
     * @param boolean $scheme if returned URL should contain website name and schema
     * @return string
     */
    public function getUrl($scheme = false) {
        return $this->publishDir($scheme) . "/{$this->file}";
    }

    /**
     * Attempts to remove current model's file and its directory
     * @return boolean
     */
    protected function removeFile() {
        try {
            FileHelper::removeDirectory($this->getDir());

            return true;
        } catch (\Exception $e) {
            \Yii::warning($e->getMessage(), 'FileBehavior');

            return false;
        }
    }

    /**
     * Saves the model and copies source file as model's file
     * important: Model should be already saved in the DB
     * @param string $source source file path
     * @param boolean $uploaded if file was uploaded via HTTP POST
     * @return boolean
     */
    public function saveFromFile($source, $uploaded = true) {
        if(!file_exists($source)) {
            throw new InvalidParamException("Source file [$source] does not exists.");
        }

        if($this->isNewRecord) {
            if(empty($this->file)) {
                $pathInfo = pathinfo($source);
                $this->file = "{$pathInfo['basename']}_" . date('Y-m-d_H-i-s');
                if($pathInfo['extension']) {
                    $this->file .= ".{$pathInfo['extension']}";
                }
            }
        } else {
            $this->removeFile();
        }

        if($this->save()) {
            $this->prepareDir();

            return $uploaded ?
                move_uploaded_file($source, $this->getPath()) :
                copy($source, $this->getPath());
        } else {
            return false;
        }
    }
}
