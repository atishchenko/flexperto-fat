<?php
/**
 * Created by PhpStorm.
 * @author: Alexey Tishchenko
 * @email: alexey@atishchenko.com
 * @UpWork: https://www.upwork.com/freelancers/~01ad7ed1a6ade4e02e
 * @date: 17.03.16
 */

namespace common\components\behaviors;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * Class RatingBehavior implements getters for next and previous models
 * ordered by some specified rating attribute
 * @package common\components\behaviors
 *
 * @property integer $id
 * @method static ActiveQuery find()
 * @method static string className()
 * @method boolean save()
 *
 * @property string $ratingAttribute
 * @property ActiveRecord $previousRated
 * @property ActiveRecord $nextRated
 */
trait RatingBehavior {

    /**
     * Returns rating-specifying attribute name
     * @return string
     */
    protected abstract function getRatingAttribute();

    /**
     * Fetches previous rated model
     * @return null|ActiveRecord
     */
    public function getPreviousRated() {
        $attribute = $this->ratingAttribute;

        $previous = static::find()
            ->where("$attribute = :rating AND id < :id")
            ->params([
                'rating' => $this->$attribute,
                'id' => $this->id,
            ])
            ->orderBy(['id' => SORT_DESC])
            ->one();
        if(!$previous instanceof static) {
            $previous = static::find()
                ->where("$attribute < :rating")
                ->params(['rating' => $this->$attribute])
                ->orderBy([$attribute => SORT_DESC, 'id' => SORT_DESC])
                ->one();
        }

        return $previous;
    }

    /**
     * Fetches next rated model
     * @return null|ActiveRecord
     */
    public function getNextRated() {
        $attribute = $this->ratingAttribute;

        $next = static::find()
            ->where("$attribute = :rating AND id > :id")
            ->params([
                'rating' => $this->$attribute,
                'id' => $this->id,
            ])
            ->orderBy(['id' => SORT_ASC])
            ->one();
        if(!$next instanceof static) {
            $next = static::find()
                ->where("$attribute > :rating")
                ->params(['rating' => $this->$attribute])
                ->orderBy([$attribute => SORT_ASC, 'id' => SORT_ASC])
                ->one();
        }

        return $next;
    }

    /**
     * Increments User profile rating counter
     */
    public function increaseRating() {
        $attribute = $this->ratingAttribute;
        ++$this->$attribute;

        return $this->save();
    }
}
