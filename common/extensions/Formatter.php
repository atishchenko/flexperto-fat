<?php
/**
 * Created by PhpStorm.
 * @author: Alexey Tishchenko
 * @email: alexey@atishchenko.com
 * @UpWork: https://www.upwork.com/freelancers/~01ad7ed1a6ade4e02e
 * @date: 16.03.16
 */
namespace common\extensions;
use sjoorm\yii\components\helpers\Misc;
use sjoorm\yii\components\helpers\PhoneNumber;
use yii\helpers\Html;
/**
 * Class Formatter implements some custom formatter rules
 * @package common\extensions
 */
class Formatter extends \yii\i18n\Formatter {

    /**
     * Formats a string as an international phone number (if valid)
     * @see sjoorm\yii\components\helpers\PhoneNumber
     * @param string $value
     * @return mixed|string
     */
    public function asPhoneNumber($value) {
        $formatted = $this->nullDisplay;

        if($value) {
            $phoneNumber = new PhoneNumber([
                'raw' => $value,
            ]);

            $formatted = $phoneNumber->isValid ?
                Html::tag(
                    'span',
                    '',
                    [
                        'class' => 'flag-icon flag-icon-' . strtolower($phoneNumber->iso),
                        'title' => Misc::getCountryName($phoneNumber->iso),
                    ]
                ) . ' ' .
                Html::a(
                    "+{$phoneNumber->code}{$phoneNumber->number}",
                    "skype:{$phoneNumber->asInternational}?call"
                ) :
                $phoneNumber->asHtml;
        }

        return $formatted;
    }
}
