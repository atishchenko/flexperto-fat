<?php
namespace common\models;

use common\components\behaviors\RatingBehavior;
use common\components\behaviors\StatusBehavior;
use sjoorm\yii\components\behaviors\ConstBehavior;
use sjoorm\yii\components\helpers\PhoneNumber;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\web\UploadedFile;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $phone
 * @property string $auth_key
 * @property integer $status
 * @property integer $views
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 *
 * @property UserAvatar $avatar
 */
class User extends ActiveRecord implements IdentityInterface
{
    use StatusBehavior;
    use ConstBehavior;
    use RatingBehavior;

    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
            // add some necessary validation rules
            ['email', 'email'],
            ['email', 'unique'],
            ['username', 'required'],
            ['username', 'unique'],
            [
                'username',
                'match',
                'pattern' => '/^[\w\d]+$/i',
                'message' => 'Your username can only contain digits and letters',
            ],
            ['phone', 'filter', 'filter' => [PhoneNumber::className(), 'purify']],
            ['views', 'integer', 'min' => 0],
            ['views', 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        $expire = \Yii::$app->params['user.passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int) end($parts);
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return \Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = \Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = \Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = \Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * Gets bootstrap statuses
     * @return array
     */
    public function getBootstrapStatuses() {
        return [
            self::STATUS_ACTIVE => 'success',
            self::STATUS_DELETED => 'danger',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAvatar() {
        return $this->hasOne(UserAvatar::className(), ['user_id' => 'id']);
    }

    /** @inheritdoc */
    public function beforeDelete() {
        if($this->avatar instanceof UserAvatar) {
            $this->avatar->delete();
        }

        return parent::beforeDelete();
    }

    /**
     * Creates new related ApplicationDocument model
     * @param UploadedFile $upload
     * @return UserAvatar
     */
    public function addAvatar(UploadedFile $upload) {
        if($this->avatar instanceof UserAvatar) {
            // remove the old one
            $this->avatar->delete();
        }

        $avatar = new UserAvatar();
        $avatar->file = 'avatar_' . date('Y-m-d_H-i-s') . ".{$upload->extension}";
        $avatar->user_id = $this->id;
        $avatar->saveFromFile($upload->tempName, true);

        return $avatar;
    }

    /**
     * Checks if current User account is active
     * @return boolean
     */
    public function isActive() {
        return $this->status == self::STATUS_ACTIVE;
    }

    /**
     * Sets User->status to "deleted" value
     * @return boolean
     */
    public function deactivate() {
        $this->status = self::STATUS_DELETED;
        return $this->save();
    }

    /**
     * Returns rating-specifying attribute name
     * @return string
     */
    protected function getRatingAttribute() {
        return 'views';
    }
}
