<?php

namespace common\models;

use common\components\behaviors\FileBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%user_avatar}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $file
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property User $user
 */
class UserAvatar extends ActiveRecord {

    use FileBehavior;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%user_avatar}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [
                [
                    'user_id',
                    'file',
                ],
                'required',
            ],
            [
                ['user_id'],
                'integer',
            ],
            [
                ['user_id'],
                'unique',
            ],
            [
                ['file'],
                'string',
                'max' => 255,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'file' => 'File',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /** @inheritdoc */
    public function beforeDelete() {
        $this->removeFile();

        return parent::beforeDelete();
    }

    /**
     * Returns URI to published files directory
     * @return string
     */
    protected function getAssetsUri() {
        return '/assets';
    }

    /**
     * Returns path to published files directory
     * @return string
     */
    protected function getAssetsPath() {
        return \Yii::getAlias('@webroot/assets');
    }

    /**
     * Returns path to data storage root directory
     * @return string
     */
    protected function getDataPath() {
        return \Yii::getAlias('@data');
    }
}
