<?php
return [
    'adminEmail' => 'admin@sjoorm.com',
    'supportEmail' => 'webmaster@sjoorm.com',
    'user.passwordResetTokenExpire' => 3600,
];
