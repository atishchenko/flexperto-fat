<?php
return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=flexperto',
            'username' => 'flexperto',
            'password' => 'flexperto',
            'charset' => 'utf8',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'mail.atishchenko.com',
                'username' => 'alerts@atishchenko.com',
                'password' => 'password-here',
                'port' => '587',
                'encryption' => 'tls',
            ],
            'viewPath' => '@common/mail',
        ],
    ],
];
