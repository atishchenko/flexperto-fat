<?php
/**
 * Yii bootstrap file.
 *
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

require(__DIR__ . '/vendor/yiisoft/yii2/BaseYii.php');

/**
 * Class AutoCompleteApplication
 * @property AutoCompleteWebUser $user
 */
class AutoCompleteApplication extends \yii\web\Application {
}

/**
 * Class AutoCompleteWebUser
 * @property \common\models\User $identity
 */
class AutoCompleteWebUser extends \yii\web\User {
}

/**
 * Yii is a helper class serving common framework functionality.
 *
 * It extends from [[\yii\BaseYii]] which provides the actual implementation.
 * By writing your own Yii class, you can customize some functionality of [[\yii\BaseYii]].
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class Yii extends \yii\BaseYii
{
    /** @var AutoCompleteApplication */
    public static $app;
}

spl_autoload_register(['Yii', 'autoload'], true, true);
Yii::$classMap = include(__DIR__ . '/vendor/yiisoft/yii2/classes.php');
Yii::$container = new yii\di\Container;
