<?php
/**
 * Created by PhpStorm.
 * @author: Alexey Tishchenko
 * @email: alexey@atishchenko.com
 * @UpWork: https://www.upwork.com/freelancers/~01ad7ed1a6ade4e02e
 * @date: 16.03.16
 */
namespace frontend\models;
use sjoorm\yii\components\helpers\Misc;
use sjoorm\yii\components\helpers\PhoneNumber;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 * Class UserDetailsForm implements form gathering and validating editable User attributes
 * @package frontend\models
 */
class UserDetailsForm extends Model {

    /** @var string which email address is an exception for "unique" validator */
    public $exceptionEmail;
    /** @var string which username is an exception for "unique" validator */
    public $exceptionUsername;

    /** @var string */
    public $username;
    /** @var string */
    public $email;
    /** @var string */
    public $code;
    /** @var string */
    public $number;
    /** @var UploadedFile */
    public $avatar;
    /** @var string */
    public $password;

    /** @var PhoneNumber */
    public $phone;

    /**
     * Gets phone codes for input dropdown
     * @return array
     */
    public function getPhoneCodes() {
        $codes = [];

        foreach(PhoneNumber::getCodes() as $iso => $code) {
            if(is_array($code)) {
                $name = Misc::getCountryName($iso);
                $subCodes = [];
                foreach($code as $subCode) {
                    $subCodes[$subCode] = "$name +$subCode";
                }
                $codes[$name] = $subCodes;
            } else {
                $codes[$code] = Misc::getCountryName($iso) . " +$code";
            }
        }

        return $codes;
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [
                'username',
                'filter',
                'filter' => 'trim',
            ],
            [
                'username',
                'required',
            ],
            [
                'username',
                'unique',
                'targetClass' => '\common\models\User',
                'message' => 'This username has already been taken.',
                'when' => function() {
                    return empty($this->exceptionUsername) || $this->username !== $this->exceptionUsername;
                },
            ],
            [
                'username',
                'string',
                'min' => 2,
                'max' => 255,
            ],

            [
                'email',
                'filter',
                'filter' => 'trim',
            ],
            [
                'email',
                'required',
            ],
            [
                'email',
                'email',
            ],
            [
                'email',
                'unique',
                'targetClass' => '\common\models\User',
                'message' => 'This email address has already been taken.',
                'when' => function() {
                    return empty($this->exceptionEmail) || $this->email !== $this->exceptionEmail;
                },
            ],
            [
                'password',
                'string',
                'min' => 6,
            ],

            // adding avatar, phone code and number
            [
                [
                    'code',
                    'number',
                    //'avatar', //not necessary on the signup step
                ],
                'required',
            ],
            [
                'code',
                'in',
                'range' => array_keys($this->getPhoneCodes()),
            ],
            [
                'avatar',
                'file',
                'maxSize' => 1024 * 1024, // 1 megabyte
                'extensions' => 'jpg, png',
                'mimeTypes' => 'image/jpeg, image/png',
            ],
            [
                'number',
                'safe',
            ],
        ];
    }

    /**
     * Generates PhoneNumber object from form input values
     * @return PhoneNumber
     */
    public function getPhoneNumber() {
        return new PhoneNumber([
            'code' => $this->code,
            'raw' => $this->number,
        ]);
    }

    /** @inheritdoc */
    public function validate($attributeNames = null, $clearErrors = true) {
        $this->phone = $this->getPhoneNumber();
        $this->avatar = UploadedFile::getInstance($this, 'avatar');

        return $this->phone->getIsValid() && parent::validate($attributeNames, $clearErrors);
    }
}
