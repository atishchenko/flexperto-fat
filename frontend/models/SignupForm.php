<?php
namespace frontend\models;

use common\models\User;
use yii\web\UploadedFile;

/**
 * Signup form
 */
class SignupForm extends UserDetailsForm {

    /**
     * @inheritdoc
     */
    public function rules() {
        return array_merge(
            parent::rules(),
            [
                [
                    'password',
                    'required',
                ],
            ]
        );
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup() {
        if($this->validate()) {
            $user = new User();
            $user->username = $this->username;
            $user->email = $this->email;
            $user->phone = $this->phone->asInternational;
            $user->setPassword($this->password);
            $user->generateAuthKey();
            if($user->save()) {
                if($this->avatar instanceof UploadedFile) {
                    $user->addAvatar($this->avatar);
                }

                return $user;
            }
        }

        return null;
    }
}
