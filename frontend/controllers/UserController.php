<?php

namespace frontend\controllers;

use frontend\models\UserDetailsForm;
use sjoorm\yii\components\helpers\Misc;
use sjoorm\yii\components\helpers\PhoneNumber;
use common\models\User;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Cookie;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * UserController implements the CRUD actions for User model.
 * Actually, only RUD actions
 */
class UserController extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => [
                    'create',
                    'update',
                ],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [
                            'delete',
                        ],
                        'verbs' => ['post'],
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true,
                        'actions' => [
                            'update',
                        ],
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true,
                        'actions' => [
                            'index',
                            'view',
                        ],
                        'verbs' => ['get'],
                        'roles' => ['*'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex() {
        $query = User::find();
        $countQuery = clone $query;
        $pagination = new Pagination([
            'totalCount' => $countQuery->count(),
            'defaultPageSize' => 8,
        ]);
        $users = $query
            ->orderBy(['views' => SORT_DESC, 'id' => SORT_ASC])
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();

        if(count($users) === 0) {
            \Yii::$app->session->addFlash('warning', 'Sorry, no users registered.');
        }

        return $this->render('index', [
            'users' => $users,
            'pagination' => $pagination,
        ]);
    }

    /**
     * Displays a single User model.
     * @param string $username
     * @return mixed
     */
    public function actionView($username) {
        $model = $this->findModel($username);
        if($model->id != \Yii::$app->user->id) {
            $key = User::className() . "#{$this->id}";

            if(!isset(\Yii::$app->request->cookies[$key]) && $model->increaseRating()) {
                $cookie = new Cookie([
                    'name' => $key,
                    'value' => true,
                    'expire' => 0, // until the browser is closed
                ]);
                \Yii::$app->response->cookies->add($cookie);
            }
        }

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $username
     * @return mixed
     */
    public function actionUpdate($username) {
        $model = $this->findModel($username);
        $phone = new PhoneNumber(['raw' => $model->phone]);
        $formModel = new UserDetailsForm([
            'username' => $model->username,
            'email' => $model->email,
            'code' => $phone->code,
            'number' => $phone->number,
            'exceptionEmail' => $model->email,
            'exceptionUsername' => $model->username,
        ]);

        if($formModel->load(\Yii::$app->request->post()) && $formModel->validate()) {
            if($formModel->avatar instanceof UploadedFile) {
                $model->addAvatar($formModel->avatar);
            }
            if($formModel->password) {
                $model->setPassword($formModel->password);
                $model->generateAuthKey();
            }
            $model->username = $formModel->username;
            $model->email = $formModel->email;
            $model->phone = $formModel->getPhoneNumber()->asInternational;

            if($model->save()) {
                return $this->redirect([
                    'view',
                    'username' => $model->username,
                ]);
            } else {
                \Yii::$app->session->addFlash('error', Misc::errorsToString($model));
            }
        }


        return $this->render('update', [
            'model' => $model,
            'formModel' => $formModel,
        ]);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $username
     * @return mixed
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     */
    public function actionDelete($username) {
        $model = $this->findModel($username);
        if(\Yii::$app->user->id != $model->id) {
            throw new ForbiddenHttpException('You are not allowed to perform this action');
        }
        // actually, we will just mark such account as "disabled"
        //$model->delete();
        $model->deactivate();

        return $this->redirect(['site/logout']);
    }

    /**
     * Finds the User model based on its username value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $username
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($username) {
        $model = User::findOne(['username' => $username]);
        if($model instanceof User) {
            return $model;
        } else {
            throw new NotFoundHttpException('The user does not exist.');
        }
    }
}
