<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var common\models\User[] $users */
/* @var \yii\data\Pagination $pagination */

$this->title = 'User profiles gallery';
$this->params['breadcrumbs'][] = 'Users';
?>
<div class="user-index">

    <div class="row">
    <?php foreach($users as $user): ?>
        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6">
            <div class="panel <?= $user->isActive() ? 'panel-success' : 'panel-danger' ?>">
                <div class="panel-heading text-center">
                    <?= $user->isActive() ? 'Active' : 'Deleted' ?>
                </div>
                <div class="panel-body">
                    <div class="avatar-thumb">
                        <?= Html::img(
                            $user->avatar instanceof \common\models\UserAvatar ?
                                $user->avatar->getUrl() :
                                \Yii::getAlias('@web/img/no-avatar.jpg'),
                            ['class' => 'avatar']
                        ) ?>
                    </div>
                    <h3 class="text-center">
                        <?= Html::a(
                            "<b>@{$user->username}</b>",
                            ['user/view', 'username' => $user->username]
                        ) ?>
                    </h3>
                </div>
            </div>
        </div>
    <?php endforeach ?>
    </div>

    <?= \yii\widgets\LinkPager::widget(['pagination' => $pagination]) ?>
</div>
