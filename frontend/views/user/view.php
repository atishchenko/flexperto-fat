<?php

use common\models\User;
use common\models\UserAvatar;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = '@' . $model->username;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
//fetch rating "neighbours"
$previous = $model->previousRated;
$next = $model->nextRated;

// hide "deleted" user details
if(!$model->isActive()) {
    $model->email = null;
    $model->phone = null;
}
?>
<div class="user-view">

    <h1>
        <?= Html::encode($this->title) ?>
        <?php if($model->id == \Yii::$app->user->id): ?>
            <span class="pull-right">
                <?= Html::a(
                    'Update your profile info',
                    ['update', 'username' => $model->username],
                    ['class' => 'btn btn-primary']
                ) ?>
                <?= Html::a('Delete', ['delete', 'username' => $model->username], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to mark your profile as disabled?',
                        'method' => 'post',
                    ],
                ]) ?>
            </span>
        <?php endif ?>
    </h1>

    <div class="row">
        <div class="col-md-3 col-sm-4 col-xs-12">
            <?= Html::img(
                $model->avatar instanceof UserAvatar ?
                    $model->avatar->getUrl() :
                    \Yii::getAlias('@web/img/no-avatar.jpg'),
                ['class' => 'avatar']
            ) ?>
        </div>
        <div class="col-md-9 col-sm-8 col-xs-12">

            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'views',
                    'username',
                    'email:email',
                    'phone:phoneNumber',
                    [
                        'label' => $model->getAttributeLabel('status'),
                        'value' => Html::tag(
                            'span',
                            $model->isActive() ?
                                'Active user' : 'Deleted user',
                            [
                                'class' => 'label label-' . $model->getStatusClass(),
                            ]
                        ),
                        'format' => 'raw',
                    ],
                    'created_at:dateTime',
                    'updated_at:dateTime',
                ],
            ]) ?>
        </div>
    </div>

    <div class="row user-gallery-buttons">
        <div class="col-xs-6 text-left">
            <?php if($previous instanceof User) {
                echo Html::a(
                    '<span class="glyphicon glyphicon-arrow-left"></span> @' . $previous->username,
                    ['view', 'username' => $previous->username],
                    ['class' => 'btn btn-default']
                );
            } else {
                echo Html::a(
                    '<span class="glyphicon glyphicon-arrow-left"></span>',
                    null,
                    ['class' => 'btn btn-default disabled']
                );
            } ?>
        </div>
        <div class="col-xs-6 text-right">
            <?php if($next instanceof User) {
                echo Html::a(
                    "@{$next->username}" . ' <span class="glyphicon glyphicon-arrow-right"></span>',
                    ['view', 'username' => $next->username],
                    ['class' => 'btn btn-default']
                );
            } else {
                echo Html::a(
                    '<span class="glyphicon glyphicon-arrow-right"></span>',
                    null,
                    ['class' => 'btn btn-default disabled']
                );
            } ?>
        </div>
    </div>
</div>
