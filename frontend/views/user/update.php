<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/** @var \frontend\models\UserDetailsForm $formModel */

$this->title = "Update User: @{$model->username}";
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = [
    'label' => "@{$model->username}",
    'url' => ['view', 'username' => $model->username]
];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-update">

    <h1>Update your profile details</h1>

    <div class="user-form">

        <?php $form = ActiveForm::begin([
            'id' => 'form-update',
            'options' => [
                'enctype' => 'multipart/form-data',
            ],
        ]); ?>

        <?= $form->field($formModel, 'username')->textInput(['maxlength' => true]) ?>

        <?= $form->field($formModel, 'email')->textInput(['maxlength' => true]) ?>

        <div class="row">
            <div class="col-sm-5 col-xs-6">
                <?= $form->field($formModel, 'code')->dropDownList($formModel->getPhoneCodes()) ?>
            </div>
            <div class="col-sm-7 col-xs-6">
                <?= $form->field($formModel, 'number')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
        <?= $form->field($formModel, 'avatar')->fileInput() ?>
        <?= $form->field($formModel, 'password')->passwordInput(['maxlength' => true])
            ->hint('Put here a new one if you want to change it') ?>

        <div class="form-group">
            <?= Html::submitButton('Update', ['class' => 'btn btn-primary']) ?>
            <?= Html::a(
                'Cancel',
                ['user/view', 'username' => $model->username],
                ['class' => 'btn btn-default']
            ) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>


</div>
