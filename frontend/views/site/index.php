<?php
/* @var $this yii\web\View */
$this->title = 'Flexperto :: Final Application Test';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Final Application Test</h1>

        <p class="lead">This is my solution for the FAT task.</p>
    </div>

    <div class="body-content">

        <h2>Overview</h2>
        <p>
            To make this application having a bit more sense than in initial task description,
            I made a gallery of user profiles sorted by their popularity (amount of "unique" profile visits).
            The gallery is under <code>user/index</code> link, which you can see in top right corner.
            Users are identified by their <code>username</code> instead of IDs.
            It is possible to review each user's profile to see some details (email address, phone number, etc.)
        </p>

        <h2>Requirements</h2>
        <p>
            Nothing special, just PHP >= version 5.4 and composer.
        </p>

        <h2>Details</h2>
        <p>
            Here in the following text I will point out some key components building application logic.
        </p>

        <h4>\frontend\models\UserDetailsForm</h4>
        <p>
            Provides basic functionality to validate user details
            and being used in editing user profile info.
            I've refactored <code>\frontend\models\SignupForm</code>
            class that it extends <code>\frontend\models\UserDetailsForm</code>
            with necessary logic without having duplicate code.
        </p>

        <h4>\common\components\behaviors\FileBehavior</h4>
        <p>
            Each uploaded user avatar belongs to the record in the <code>user_avatar</code> table
            represented by <code>\common\models\UserAvatar</code> model that uses <code>FileBehavior</code>,
            implementing file system operations. Each avatar file is stored in predefined directory
            <code>data</code> under <code>common-models-UserAvatar</code> subdirectory
            and being published (via creating symbolic link) to the application <code>assets</code>.
            The file is being deleted together with its <code>UserAvatar</code> record.
        </p>

        <h4>\common\components\behaviors\RatingBehavior</h4>
        <p>
            This trait provides rating functionality to a User model.
            Each unique visit of <code>user/view</code> page increments <code>User->views</code> counter
            using <code>User->increaseRating()</code> method of this behavior.
            When someone loads the page a cookie is being created and sent back to visitor's browser.
            The existence of this cookie shows that the visitor had already "voted" for a user.
        </p>

        <h4>\sjoorm\yii\components\helpers\PhoneNumber</h4>
        <p>
            This is one of my own components that are available via composer. In this project
            I've used it to filter (<code>PhoneNumber::purify</code>) input data from user, validate it
            and fill in <code>User->phone</code> value.
            It is also being used to make custom <code>\common\extensions\Formatter</code>
            extension which allows to print out phone number as a link in international format with
            a flag of country where it belongs to. You can see it at <code>user/view</code> page.
        </p>
    </div>
</div>
