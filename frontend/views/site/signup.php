<?php
use sjoorm\yii\components\helpers\Misc;
use sjoorm\yii\components\helpers\PhoneNumber;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

$this->title = 'Signup';
$this->params['breadcrumbs'][] = $this->title;

$codes = [];
foreach(PhoneNumber::getCodes() as $iso => $code) {
    if(is_array($code)) {
        $name = Misc::getCountryName($iso);
        $subCodes = [];
        foreach($code as $subCode) {
            $subCodes[$subCode] = "$name +$subCode";
        }
        $codes[$name] = $subCodes;
    } else {
        $codes[$code] = Misc::getCountryName($iso) . " +$code";
    }
}
?>
<div class="site-signup">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Please fill out the following fields to signup:</p>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin([
                'id' => 'form-signup',
                'options' => [
                    'enctype' => 'multipart/form-data',
                ],
            ]); ?>
                <?= $form->field($model, 'username') ?>
                <?= $form->field($model, 'email') ?>
                <div class="row">
                    <div class="col-sm-5 col-xs-6">
                        <?= $form->field($model, 'code')->dropDownList($codes) ?>
                    </div>
                    <div class="col-sm-7 col-xs-6">
                        <?= $form->field($model, 'number')->textInput(['maxlength' => true]) ?>
                    </div>
                </div>
                <?= $form->field($model, 'avatar')->fileInput()->hint('You can upload it later') ?>
                <?= $form->field($model, 'password')->passwordInput() ?>
                <div class="form-group">
                    <?= Html::submitButton('Signup', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
